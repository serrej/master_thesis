# This a general README and there is a more specific README for part 3.

The idea of this project is to give a method that explains how to pass from raw text to extraction relation.

## Part1

For this part we have to use PYTHON3 

We need to install Gensim Framework

This code is used to clean a corpus from Wikipedia dump : https://dumps.wikimedia.org/backup-index.html

Run the script : python3 1_process_wiki.py inputFile.xml.bz2 outputFile

After this part we will obtain a clean corpus with all the letters in lowercase, all the accents replaced by the same letter without an accent, and finally remove all the non-ASCII character

## Part2

For this part we use Python 2.7, PySpark 2(Spark). 

This code is used to clean the corpus and use it to train a word2vec model. 
Moreover you can fit your model. More details in https://spark.apache.org/docs/2.1.0/api/java/org/apache/spark/mllib/feature/Word2Vec.html


Run the script : 

* Local : spark-submit  --driver-memory 16G --executor-memory 16G 2_Mllib_Save_Load_Spark.py
* Cluster : spark-submit --conf spark.driver.maxResultSize=6G --master yarn --deploy-mode cluster  --driver-memory 42G --executor-memory 35G --num-executors 10 --executor-cores 4  2_Mllib_Save_Load_Spark.py 

Notice : the configuration in Cluster mode depends on your cluster.

## Part3  

The idea of this project is to rely on links between two words to extract a new relation of the same type (e.g. : «Paris - France» in input, to extract a city - country relation «Bern - Switzerland»). We can extract semantic relationship. This code works on Spark. The original code : https://github.com/matus-pikuliak/word-embeddings.

For more explanation see inside the folder and read the specific readme for this part.

## Part4

This part is used to evaluate the relations extracted by word2vec. For that we use  relations extracted from Knowledge base in order to compare them.

We need to install math, argparse, json

Firstly, we have to get JSON file which contains the relations to evaluate. Use this tool to extract it with SPARQL Query: https://query.wikidata.org 
Each Json file must be respect the same structure(example for Country - Currency relation) : 

{"id":"http://www.wikidata.org/entity/Q17","sub":"Japan","obj":"Japanese yen"}

To evaluate our relation we use percentage and nDCG. 

Run the script : 

python evaluate.py part-00000_cities15 -j query_City_Country.json -o output.txt

There are 3 arguments : 
* InputFile : "part-00000_cities15" is the output file from part 3
* -j : Json file from Wikidata KB
* -o : output file

# Optional 

There are 2 optional scripts :

* One is to convert a MLLIB WORD2VEC Model to Gensim WORD2VEC Model :
So we need to install Gensim and Spark.

Run the script :
spark-submit --conf spark.driver.maxResultSize=4G --executor-memory 24G  --driver-memory 24G Optional_Millib_Model_To_Gensim_Model.py 
You need to change the value "pathNameFileModel" inside the script to select our model

* The second script was used to have a visualization in 2D of our model.
But we need to have a Gensim word2vec model in text format. 

Run the script : python visualization.py TextWord2VecGensimModel.txt


## More details in my Master's Thesis Report in PDF
