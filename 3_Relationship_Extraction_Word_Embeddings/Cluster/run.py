#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
from pyspark import SparkContext, SparkConf
from pyspark.mllib.clustering import KMeans, KMeansModel
from class_container import *
from numpy import array
import collections


# Requirements
# pip install --user -U argparse
# pip install --user -U numpy
# pip install --user -U scipy
# pip install --user -U psutil

# USE SPARK 2 if possible
# export SPARK_MAJOR_VERSION=2

############################
#       Parsing Part       #
############################

parser = argparse.ArgumentParser()
parser.add_argument("input",
                    help="filename of seed set file")
parser.add_argument("-o", dest='output',
                    help="filename where results will be written")
parser.add_argument("-t", dest='result_count',
                    type=int, default=100,
                    help="number of results returned")
parser.add_argument("-n", dest='neighborhood',
                    type=int, default=100,
                    help="number of neighbours selected when generating candidates")
parser.add_argument("-s", dest='selection',
                    type=bool, default=False,
                    help="True means selection and False non selection")
args = parser.parse_args()


############################
#       Class Part         #
############################

class PairSet:
    """
    This class represents set of pairs, usually seed set used to generate new pairs. It can calculate various metrics
    concerning such sets as well as generate new pairs in accordance with our method.
    """

    def __init__(self, pairs, filename=None):
        """
        filename is a path to file, if the set was created from one.
        :param pairs: list of Pairs
        :param filename: string
        """
        self.set_pairs = pairs
        self.filename = filename

    @classmethod
    def create_from_file(cls, filename):
        """
        Takes files in our input format (see README) and transforms it into PairSet object.
        :param filename: string
        :return: PairSet
        """
        return PairSet(load_input_file(sc, filename), filename=filename)

    def kmeans_selection(self, k):
        """
        This function is used to selected the pairs in input by kmeans kmeans_selection,
        with k for the number of group
        :param k: integer
        :return: None
        """
        dict_pair = {}
        set_p = self.set_pairs

        # Dictionary of the seed pairs
        for i in range(0, len(set_p)):
            dict_pair[set_p[i].word()] = set_p[i]

        # List of list of vector used in kmeans algorithm
        listOfVector = []
        for key in dict_pair.keys():
            listOfVector.append(np.asarray(dict_pair[key].pair_embedding.v))

        size = len(dict_pair)
        # Transfrom the list to RDD
        listOfVectorRDD = sc.parallelize(listOfVector)
        # Train our Kmeans model
        clusters = KMeans.train(listOfVectorRDD, k, maxIterations=size, initializationMode="k-means||", seed=42)
        # list of tuple of each candidates with this word and vector representation in rdd
        listOfCandidates = []
        for key in dict_pair.keys():
            rddVector = sc.parallelize([np.asarray(dict_pair[key].pair_embedding.v)])
            word = dict_pair[key].pair_embedding.word
            listOfCandidates.append((word, rddVector))

        # We create a dictionnary wich contain each cluster with the candidates
        dictio = {}
        for w,rddV in listOfCandidates:
            v = rddV.collect()
            # Return the group of this vector
            res = clusters.predict(v[0])
            # Compute the sum of squared distances of points to their nearest center with RDD 
            sse = clusters.computeCost(rddV)
            if(res in dictio.keys()):
                tmp = dictio[res]
                dictio[res] = dictio[res] + [(w, sse)]
            else: 
                dictio[res] = [(w, sse)]

        # Sort each cluster by the sum of squared distances of points to their nearest center 
        for key in dictio.keys():
            tmp = dictio[key]
            res = sorted(tmp, key=lambda x: x[1], reverse=False)
            dictio[key] = res

        # Print each groupe in sorted order
        # for k in dictio.keys():
        #     print(dictio[k])

        # Print the result, if we want to take 5 we take the 5 first. 
        i = 0
        while(len(dictio)>0):
            for key in dictio.keys():
                res = dictio[key]
                if res != None:
                    i = i + 1
                    print(str(i) + ") " + res[0][0])
                    #print(res[0][0])
                    res.pop(0)
                    if(len(res) == 0):
                        del dictio[key]
                    else: 
                        dictio[key] = res

    def pair_selection(self, sizeGrp = 5 , numGrp = 5, eucliT_cosF = False):
        """
        This function is used to selected the pairs in input by euclidean or cosine similarity
        Group similar element in a group of same size in order. Moreover we sort each element in group
        to obtain a rank of the most similar element. And we print the result in order to selected the
        candidates. 
        :param emb_1: Embedding
        :param emb_2: Embedding
        :return: None
        """
        dict_pair = {}
        set_p = self.set_pairs
        # We load the seed pairs
        for i in range(0, len(set_p)):
            dict_pair[set_p[i].word()] = set_p[i]
        original = dict_pair.keys()

        # We transform in RDD
        rdd = sc.parallelize([dict_pair.keys()]) 
        #rdd = sc.parallelize([["A","B","C","D","E","F"]])

        # we have to obtain a list of list in order to send a list
        res = rdd.flatMap(lambda x: combinations(x, sizeGrp))

        # We collect all the combinations possible
        resC = res.collect()

        # Now we want to get the score of each group in order to select the best groups
        group_score = []
        results = 0
        for sublist in resC:
            list_pair = []
            for pair_word in sublist:
                list_pair.append(dict_pair[pair_word])
            # We will obtain the sum of the similarity score for each element of the group
            results = group_similarity(list_pair, eucliT_cosF)
            group_score.append([sublist, results])

        # We sort the group (list of list)
        group_score = sorted(group_score, key=lambda x: x[1], reverse=True)

        stop = 0
        final = []
        # Each group contains a list of its elements and its group score
        for liste, s in group_score:
            check = True
            # We check if we have already take a pair in group alerady selected
            for pair in liste:
                if pair not in original:
                    check = False
            if(check):
                print(liste)
                #print(' - '.join(liste))
                print("Score : " + str(s))
                final.append([liste, s])
                stop = stop + 1
                # Here we remove all pair taken in a group in order to avoid to select it again
                for elem in liste :
                    #print("-> " + elem)
                    original.remove(elem)
            # When we obtain the number of group wanted we stop
            if stop == numGrp:
                break

        # Now we obtain final list and we want to chose the represent of each group
        # We call seed similarity to compute the similarity and print it 
        for liste_tmp, s in final:
            list_pair = []
            for pair in liste_tmp: 
                list_pair.append(dict_pair[pair])
            elements_similarityPrint(list_pair, eucliT_cosF)
        #print (dict_pair)



    def __len__(self):
        """
        Number of pairs in set.
        :return: integer
        """
        return len(self.set_pairs)

    def spatial_candidates(self, size=100):
        """
        Generates candidates in accordance to the proposal of our method. Candidates are calculates as a union
        of candidates generated from individual pairs (see Pair.spatial_candidates()).
        This function return a dictionary that contains all the candidates
        :param size: integer
        :return: Dictionary
        """

        candidates = sc.emptyRDD()
        for pair in self.set_pairs:
            candidates = candidates.union(pair.neighbours(sc, size))

        candidates = candidates.collectAsMap()

        return candidates

    def find_new_pairs(self, result_count=100, output=args.output, neighborhood=100):
        """
        Finds new pairs from given set and prints them to file. result_count is number of results printed to file
        with a name set by parameter output. Call spatial candidates function
        :param result_count: integer
        :param output: string
        :param neighborhood : integer
        :return: None
        """

        seed = self
        candidates = seed.spatial_candidates(neighborhood)
        weights = [float(1) / len(seed) for _ in xrange(len(seed))]
        pairsNameInput = []
        for p in seed.set_pairs:
            pairsNameInput.append(p.pair_embedding.word)

        results = ResultList()
        for pair in candidates.values():
            if pair.pair_embedding.word not in pairsNameInput:
                positive = pair.positive
                name = pair.word()

                similarities = [pair.euclidean_similarity(seed_pair) for seed_pair in seed.set_pairs]

                tmp = [weights[i] * similarities[i] for i in xrange(len(similarities))]

                final_similarity = sum(tmp)

                results.append(is_positive=positive, name=name, score=final_similarity)
            else:
                print(pair.pair_embedding.word)

        results.print_top_n_to_file(result_count, output)


class Result:
    """
    One result from our experiment consisting of pair, its score, position
    and information about whether it is positive or not.
    """

    def __init__(self, name=None, is_positive=None, score=None, position=None):
        """
        name is the pairs of words from given pair e.g. "Paris France".
        is_positive says whether the result is from positive sample or not (candidate).
        score says how was given pair scored by scoring algorithm.
        position says what is the position of result in given result list.
        :param name: string
        :param is_positive: True or False
        :param score: float
        :param position: integer
        """
        self.name = name
        self.is_positive = is_positive
        self.score = score
        self.position = position


class ResultList:
    """
    List of Results from given experiment. It contains numerous Results and it is capable of working them,
    e.g. sorting them and printing them.
    """

    def __init__(self):
        self.results_list = []

    def __len__(self):
        return len(self.results_list)

    def __getitem__(self, item):
        return self.results_list[item]

    def append(self, **kwargs):
        """
        Appends new result to list of results. Allowed arguments are listed in Result class definition.
        :param kwargs: See Result class
        :return: None
        """
        result = Result(**kwargs)
        self.results_list.append(result)

    def sort(self):
        """
        Sorts results in result list and assigns them positions starting with 1.
        :return: None
        """
        self.results_list = sorted(self.results_list, key=lambda result: -result.score)
        for i in xrange(len(self)):
            self[i].position = i + 1

    def print_top_n_to_file(self, n, filename):
        """
        Prints names of top n results to file called filename in accordance with out result file format.
        See README for more details.
        :param n: integer
        :param filename: string
        :return: None
        """
        self.sort()
        l = []
        #l = ["?\t%s\n" % self[i].name for i in xrange(n)]
        for i in xrange(n):
            #x = "?\t%s\n" % self[i].name
            x = "%s\n" % self[i].name
            #x = "%s" % self[i].name + " : " + str(self[i].score)
            l.append(x)
            print(x.encode('utf-8'))
        if filename != None:
            output = sc.parallelize(l).coalesce(1)
            output.saveAsTextFile(filename)

############################
#       Run Part           #
############################

#conf = SparkConf().setAppName("RE_Word_Embeddings").set("spark.driver.maxResultSize", "4G")

#sc = SparkContext(conf=conf)
sc = SparkContext(appName="RE_Word_Embeddings")

# Less verbose
sc.setLogLevel("ERROR")

#modelPath = "model/enwiki_ASCII_K100_PART4_ALPHA05_iter1_Min5_Win10.model"
#modelPath = "model/enwiki_ASCII_K100_PART4_ALPHA05_iter1_Min5_Win10_NGram_cleanned.model"
modelPath = "hdfs:///user/jeremy/model/mllib/enwiki_ASCII_K100_PART4_ALPHA05_iter1_Min5_Win10.model"

load_model(sc, modelPath)
seed_set = PairSet.create_from_file(args.input)

if(args.selection):
    # For euclidean or cosine input selection change eucliT_cosF
    seed_set.pair_selection(sizeGrp = 5, numGrp = 5, eucliT_cosF = True)
    # For k-means input selection
    #seed_set.kmeans_selection(k = 5)
else:
    seed_set.find_new_pairs(output=args.output,
                           result_count=args.result_count,
                           neighborhood=args.neighborhood)

# For euclidean or 
#seed_set.pair_selection(sizeGrp = 5, numGrp = 5, eucliT_cosF = True) # 3 et 8 = 24
#seed_set.kmeans_selection(k = 5)


sc.stop()

# Local
# time spark-submit --conf spark.driver.maxResultSize=4G --driver-memory 16G --executor-memory 16G run.py relations/cities15.txt -o output/Output_cities15.txt
# For selection :
# time spark-submit --conf spark.driver.maxResultSize=4G --driver-memory 16G --executor-memory 16G run.py relations/cities15.txt -o output/Output_cities15.txt -s True
# Cluster
# time spark-submit --py-files class_container.py --conf spark.driver.maxResultSize=4G --master yarn --deploy-mode cluster  --driver-memory 42G --executor-memory 35G --num-executors 84 --executor-cores 4  run.py relations2/relations2/Genre25.txt  -o hdfs:///user/jeremy/relations2/Genre25_output
