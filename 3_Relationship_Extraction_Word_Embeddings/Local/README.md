# Relation Extraction With SPARK in Local Mode

The idea of this project is to rely on links between two words to extract a new relation of the same type (e.g. : «Paris - France» in input, to extract a city - country relation «Bern - Switzerland»). We can extract semantic relationship. This code works on Spark. The original code : https://github.com/matus-pikuliak/word-embeddings.

## Installation

We use python 2.7

pip install --user -U argparse
pip install --user -U numpy
pip install --user -U scipy
pip install --user -U psutil

We also need to have Spark(https://spark.apache.org/downloads.html).

## Input file

The input file contains some pairs in order to extract similar pairs. So we have to create a file with some pairs inside like : 

athens greece
baghdad iraq
bangkok thailand

Please refer to the folder "relations" in order to see more examples.

## Run 

To launch our programme we have to use run.py 

There are 4 arguments :

* Input : The text file which contains the input pair (mandatory)
* Output : -o output.txt (mandatory)
* Number of Pairs Return : -t set to 100 by default (optional)
* Number of neighbours selected when generating candidates : -n set to 100 by default (optional)

Examples with Spark : 
* spark-submit --conf spark.driver.maxResultSize=4G --driver-memory 16G --executor-memory 16G run.py relations/cities15.txt -o output/Output_cities15.txt

* time spark-submit --conf spark.driver.maxResultSize=4G --driver-memory 16G --executor-memory 16G run.py relations/cities15.txt -t 100 -n 100 -o output/Output_cities15.txt

