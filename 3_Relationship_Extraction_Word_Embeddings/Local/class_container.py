#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
from scipy.spatial import distance
from pyspark.mllib.feature import Word2VecModel


embeddings = dict()
euclidean = dict()
cosines = dict()
model = None


# ###############################
# #       Function  Part        #
# ###############################

def load_model(sc, modelPath):
    """
    Loads model of language as set of vectors and assign to a global value
    :param sc: SparkContext
    :param modelPath: string
    """
    global model
    model = Word2VecModel.load(sc, modelPath)


def load_input_file(sc, filename):
    """
    Takes files in our input format and transforms it into PairSet object.
    :param sc: SparkContext
    :param filename: string
    :return: List of Pair
    """

    pairs = sc.textFile(filename).map(lambda x: x.split()).collect()

    seed_pairs = list()
    for pair in pairs:
        if (model.transform(pair[0]) is not None) and (model.transform(pair[1]) is not None):
            seed_pairs.append(Pair(embedding_object(pair[0]), embedding_object(pair[1]), positive=True))
    return seed_pairs


def embedding_object(word=None, vector=None):
    """
    This method serves as a interface to embedding cache. If the embedding with given word was already
    used it will return this object. Otherwise it will create new object with specified vector.
    :param word: string
    :param vector: list of floats
    :return: Embedding
    """
    if word not in embeddings:
        embeddings[word] = Embedding(word=word, vector=vector)

    return embeddings[word]

def euclidean_similarity(emb_1, emb_2):
    """
    Calculates euclidean similarity between emb_1 and emb_2 Embedding objects. It will store the calculations
    and if the same embeddings are compared again it will return this saved result.
    :param emb_1: Embedding
    :param emb_2: Embedding
    :return: float
    """
    if emb_2 in euclidean and emb_1 in euclidean[emb_2]:
        return euclidean[emb_2][emb_1]
    if emb_1 in euclidean and emb_2 in euclidean[emb_1]:
        return euclidean[emb_1][emb_2]
    if emb_1 not in euclidean:
        euclidean[emb_1] = dict()
    euclidean_value = 1 / (1 + distance.euclidean(emb_1.v, emb_2.v))

    euclidean[emb_1][emb_2] = euclidean_value

    return euclidean_value

def cosine_similarity(emb_1, emb_2):
    """
    Calculates cosine similarity between emb_1 and emb_2 Embedding objects. It will store the calculations
    and if the same embeddings are compared again it will return this saved result.
    :param emb_1: Embedding
    :param emb_2: Embedding
    :return: float
    """
    if emb_2 in cosines and emb_1 in cosines[emb_2]:
        return cosines[emb_2][emb_1]
    if emb_1 in cosines and emb_2 in cosines[emb_1]:
        return cosines[emb_1][emb_2]
    if emb_1 not in cosines:
        cosines[emb_1] = dict()
    cosine_value = -distance.cosine(emb_1.v, emb_2.v) / 2 + 1
    cosines[emb_1][emb_2] = cosine_value
    return cosine_value

def combinations(list_elem, n):
    """
    Generates all combinations possible, list_elem contains all the elements and n is the size
    of group. 
    :param emb_1: list
    :param n: Integer
    :return: list
    """
    globalList = []
    if (n > len(list_elem)):
        return []
    else:
        if n == 1:
            for elem in list_elem:
                globalList.append([elem])
            return(globalList)
        elif len(list_elem) > 1 :
            head = list_elem[0]
            tail = list_elem[1:]
            tlCombination = combinations(tail, n - 1)
            tlCombinationMapped = []
            for list_n in tlCombination:
                tlCombinationMapped.append([head] + list_n)
            globalList = tlCombinationMapped + combinations(tail, n)
            return globalList
        else:
            return []

def seed_similarity(liste, eucliT_cosF=False):
    """
    Calculates cosine similarity or euclidean_similarity it depend of the value of eucliT_cosF.
    :param liste: list
    :eucliT_cosF: boolean
    :return: list of tuple
    """
    size = len(liste)
    weights = [float(1) / size for _ in xrange(size)]
    results = []

    # Results contains a list of result of each element with groupe
    for pair in liste:
        name = pair.word()
        if (eucliT_cosF):
            similarities = [pair.euclidean_similarity(seed_pair) for seed_pair in liste if seed_pair != pair]
        else:
            similarities = [pair.cosine_similarity(seed_pair) for seed_pair in liste if seed_pair != pair]
        tmp = [weights[i] * similarities[i] for i in xrange(len(similarities))]
        results.append([name, tmp])
    return(results)

def group_similarity(liste, eucliT_cosF=False):
    """
    This function is used to compute euclidean/cosines similarity for a group
    :param liste: list
    :eucliT_cosF: boolean
    :return: integer
    """
    results = seed_similarity(liste, eucliT_cosF)
    final_similarity = 0 
    for w, score in results:
        final_similarity = final_similarity + sum(score)
    res = final_similarity/len(liste)
    return (res)

def elements_similarityPrint(liste, eucliT_cosF=False):
    """
    This function is used to compute euclidean/cosines similarity for each elements
    of group and sorted by best score. 
    :param liste: list
    :eucliT_cosF: boolean
    """
    results = seed_similarity(liste, eucliT_cosF)
    res = []
    for w, score in results:
         res.append([w, sum(score)])
    res = sorted(res, key=lambda x: x[1], reverse=True)
    i = 0
    for elem in res:
        i=i+1
        print(str(i) + ") " + elem[0])
    print('\n')

# #################################
# #       Class  Embedding        #
# #################################


class Embedding:
    """
    This class represents one word embedding as word and the vector it is assigned. These objects are usually
    not created directly but only through embedding_object() interface.
    """

    def __init__(self, word=None, vector=None):
        """
        :param word: string
        :param vector: list of floats
        """

        if vector is None and word is None:
            raise KeyError('You have to state word or vector')

        self.v = np.array(model.transform(word)) if vector is None else vector
        self.word = word

    def __len__(self):
        """
        Length of vector
        :return: integer
        """
        return len(self.v)

    def __sub__(self, embedding):
        """
        Calculates substraction of self and given embedding as new Embedding. Also uses embedding_object interface.
        :param embedding: Embedding
        :return: Embedding
        """
        if len(self) != len(embedding):
            raise KeyError('The embeddings have different lengths')

        vector = [self.v[i] - embedding.v[i] for i in xrange(len(self))]
        name = "%s - %s" % (embedding.word, self.word)

        return embedding_object(vector=vector, word=name)

    def neighbours(self, size=100):
        """
        Finds of given word neighbours with size n in vector space. Returns list of words.
        :param size: integer
        :return: list of strings
        """
        return [embedding_object(record[0]) for record in model.findSynonyms(self.word, size)]

# ############################
# #       Class  Pair        #
# ############################


class Pair:
    """
    This class represents pair of embeddings forming one pair with semantic relation. Objects of this class can
    calculate similarity between themselves and can also be transformed to SVM-format string.
    """

    def __init__(self, embedding_1, embedding_2, positive=False, candidate=False):
        """
        Pair is created from two embeddings (1 & 2). positive and candidate are boolean variables indicating
        status of pair. At least one of them should be False.
        :param embedding_1, embedding_2: Embedding
        :param positive: True or False
        :param candidate: True or False
        """
        self.e_1 = embedding_1
        self.e_2 = embedding_2
        self.pair_embedding = embedding_2 - embedding_1
        self.positive = positive
        self.candidate = candidate

    def __len__(self):
        """
        Length of pair embedding vector
        :return: integer
        """
        return len(self.pair_embedding)

    def word(self):
        """
        Word assigned to given pair, it has a format of word1-word2, e.g. "Paris-France"
        :return: string
        """
        return self.pair_embedding.word

    def euclidean_similarity(self, pair):
        """
        Euclidean similarity between self and some other pair. It is using data helper to cache results.
        :param pair: Pair
        :return: float
        """
        return euclidean_similarity(self.pair_embedding, pair.pair_embedding)

    def cosine_similarity(self, pair):
        """
        Cosine similarity between self and some other pair. It is using data helper to cache results.
        :param pair: Pair
        :return: float
        """
        return cosine_similarity(self.pair_embedding, pair.pair_embedding)

    def neighbours(self, sc, size=100):
        """
        Generates candidates for this given pair as product of neighborhood of its two embeddings.
        :param sc: SparkContext
        :param size: Integer
        :return: list of Pairs in RDD type
        """
        ng_1 = sc.parallelize(self.e_1.neighbours(size))
        ng_2 = sc.parallelize(self.e_2.neighbours(size))
        rdd = ng_1.cartesian(ng_2).cache()
        return rdd.flatMap(lambda x: [(x[0].word+'-'+x[1].word, Pair(x[0], x[1],
                                                                  candidate=True))] if x[0].word != x[1].word else [])

