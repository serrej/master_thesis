#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import re
import argparse
import math

parser = argparse.ArgumentParser()
parser.add_argument("input",
                    help="filename to evaluate")
parser.add_argument("-j", dest='json',
                    help="filename contains true relations extraction")
parser.add_argument("-o", dest='output',
                    help="filename where results will be written")
args = parser.parse_args()

# python evaluate.py part-00000_Country_Capitals_K100_2 -j query_City_Country.json -o output.txt
# time python evaluate.py part-00000_Country_Capitals_K300_Part2_2 -j query_City_Country.json -o output.txt

print(args.input)
print(args.json)
print(args.output)

class Evaluation:

    def __init__(self): 
        self.data = {}
        self.keys = []
        self.score = []

    def __len__(self):
        return len(self.data)

    def load_json(self, input_json):
        '''
        Function for load json file in the class
        '''
        self.data = {}
        with open(input_json) as json_file:
            json_data = json.load(json_file)

        for i in range(0,len(json_data)):
            subject = json_data[i]["sub"]
            obj = json_data[i]["obj"]
            if subject in self.data.keys():
                self.data[subject].append(json_data[i]["obj"])
            else:
                self.data[subject] = [json_data[i]["obj"]]
        self.keys = self.data.keys()

    def match_pair(self, pair): 
        '''
        Function call in evaluate function in order to find if some candidates match
        Return string
        '''
        p0 = pair[0]
        p1 = pair[1]
        # For N-Gram
        if "_" in p0:
            res = p0.split("_")
            p0 = ' '.join(res)
        if "_" in p1:
            res = p1.split("_")
            p1 = ' '.join(res)
        r=re.compile(".*("+p0+").*")
        candidates = list(set(filter(r.match, self.keys)))
        for candidate in candidates:
            for result in self.data[candidate]:
                if result == p1 :
                        return(p0 + " - " + p1 + " : " + candidate + " - " + result)
                elif re.findall(".*("+p1+").*", result):
                        return(p0 + " - " + p0 + " : " + candidate + " - " + result) 
        return None

    def evaluate(self, input_file, output_file):
        '''
        Function used to compare and evaluate the input file with real relation in order to know
        if theses relations are correct
        Return List of string
        '''
        inputFile = open(input_file)
        outputFile = open(output_file, 'w')
        content = inputFile.readlines()
        listOfRes = []
        for c in content:
            pair = c.title().strip().split(' - ')
            if len(pair) == 2:
                res = self.match_pair(pair)
                if res != None:
                    res = res.encode('utf-8') 
                    print res
                    listOfRes.append(res)
                    outputFile.write('1 ' + c.strip() + ' ' + res + '\n')
                    self.score.append('1')
                else:
                    outputFile.write('0 ' + c.strip() + '\n')
                    self.score.append('0')

        return (listOfRes)

def computation_precision(numPositive, numPair = 100):
    '''
    Function used to compute the precision
    numPositive is the number of good candidate
    Return Nothing
    '''
    preci = numPositive * 100 / numPair
    preci = float(numPositive) / (float(numPair))
    print("Precision : " + str(preci))

def computation_precision_load(filename):
    '''
    Function used to compute the precision for file already evaluated
    Return Nothing
    '''
    inputFile = open(filename)
    size = 0 
    nbe = 0 
    for c in inputFile.readlines():
        size = size + 1 
        if int(c[:1]) != 0 : 
            nbe = nbe + 1

    preci = float(nbe) / (float(size))
    print("Precision : " + str(preci))


def ndcg(list_score):
    resDCG = 0
    resIDCG = 0
    for i in range(1, len(list_score)+1):
        val = float(list_score[i-1])
        if val : 
            # DCG computation 
            resDCG = resDCG + (val / math.log(i+1, 2))
    print("DCG Result : " + str(resDCG))
    reverseListScore = sorted(list_score,reverse=True) 
    for i in range(1, len(reverseListScore)):
        val = float(reverseListScore[i-1])
        if val : 
            # DCG computation 
            resIDCG = resIDCG + (val / math.log(i+1, 2))
    print("IDCG Result : " + str(resIDCG))
    nDCG = resDCG / resIDCG 
    print("nDCG Result : " + str(nDCG))
    return(nDCG)

def load_score(filename):
    inputFile = open(filename)
    return([int(c[:1]) for c in inputFile.readlines()])


# Evaluation
x = Evaluation()
x.load_json(args.json)
res = x.evaluate(args.input, args.output)
computation_precision(len(res), 100)
ndcg(x.score)

# Evaluation a file already evaluates
# ndcg(load_score("Genre20kmeansModel1_output"))
# computation_precision_load("Genre20kmeansModel1_output")

