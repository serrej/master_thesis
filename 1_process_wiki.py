#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Author: Pan Yang (panyangnlp@gmail.com)
# Copyrigh 2017

# Modify : Jérémy Serre

# time python3 1_process_wiki.py /Users/Jerem/Desktop/NLP/Bis/Word2Vec/Original/enwiki-latest-pages-articles.xml.bz2 enwiki-latest-pages-articles-V2.txt

from __future__ import print_function

import logging
import os.path
import six
import sys
import re
import unicodedata
import re

################################
##### WARNING USE PYTHON 3 #####
################################

from gensim.corpora import WikiCorpus
from gensim.models import Phrases

if __name__ == '__main__':
    program = os.path.basename(sys.argv[0])
    logger = logging.getLogger(program)

    logging.basicConfig(format='%(asctime)s: %(levelname)s: %(message)s')
    logging.root.setLevel(level=logging.INFO)
    logger.info("running %s" % ' '.join(sys.argv))

    # check and process input arguments
    if len(sys.argv) != 3:
        print("Using: python3 1_process_wiki_ngram.py enwiki.xxx.xml.bz2 wiki.en.text")
        sys.exit(1)
    inp, outp = sys.argv[1:3]
    space = " "
    i = 0

    # Load Stop List word 
    stoplist = set([line.strip() for line in open("StopwordLists.txt", 'r')])

    output = open(outp, 'w')
    wiki = WikiCorpus(inp, lemmatize=False, dictionary={})
    i = 0
    for text in wiki.get_texts():
        sentence =[]
        # https://stackoverflow.com/questions/2152898/filtering-a-list-of-strings-based-on-contents
        listeSentence = []
        #sentence = [word for word in text if word not in stoplist]
        for word in text:
            tmp=[]
            # https://stackoverflow.com/questions/15261793/python-efficient-method-to-replace-accents-é-to-e-remove-a-za-z-d-s-and
            # s='éôéèàîoôÖù@&ñóÁÉÍÓÚÝ' -> 'eoeeaiooOu@&noAEIOUY'
            # We use decode because with wiki.get we obtain b'word' so the words was in bytes and we want to transform to string
            for c in unicodedata.normalize('NFD', word):
                if unicodedata.category(c) != 'Mn':
                    tmp.append(c)
            w = ''.join(tmp)

            if w not in stoplist:
                sentence.append(word)

        listeSentence.append(sentence)
        bigram = Phrases(listeSentence, min_count=10, threshold=100)
        phrases = list(bigram[listeSentence])
        trigram = Phrases(bigram[listeSentence], min_count=10, threshold=100)
        phrases = list(trigram[bigram[listeSentence]])

        #Flatten
        for c in phrases :
            final = ' '.join(c)

        # Remove non ASCII
        final = final.encode("ascii", errors="ignore").decode() + "\n"
        # replace 2 or more space by one, the cause of that is the encode and decode
        output.write(re.sub(' +',' ',final))

        i = i + 1
        if (i % 10000 == 0):
            logger.info("Saved " + str(i) + " articles")

    output.close()
    logger.info("Finished Saved " + str(i) + " articles")