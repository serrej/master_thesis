#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pyspark import SparkContext, SparkConf
from pyspark.mllib.feature import Word2Vec
from pyspark.mllib.feature import Word2VecModel

# export SPARK_MAJOR_VERSION=2

# Local
# time spark-submit --conf spark.driver.maxResultSize=6G --driver-memory 16G --executor-memory 16G 2_Mllib_Save_Load_Spark.py
# Cluster
# time spark-submit --conf spark.driver.maxResultSize=6G --master yarn --deploy-mode cluster  --driver-memory 42G --executor-memory 35G --num-executors 10 --executor-cores 4  2_Mllib_Save_Load_Spark_3.py 

# Function getAnalogy from : https://stackoverflow.com/questions/34172242/spark-word2vec-vector-mathematics
def getAnalogy(s, model):
    qry = model.transform(s[0]) - model.transform(s[1]) - model.transform(s[2])
    res = model.findSynonyms((-1)*qry,5) # return 5 "synonyms"
    res = [x[0] for x in res]
    for k in range(0,3):
        if s[k] in res:
            res.remove(s[k])
    return res[0]

############################
#       Config Part        #
############################

# Full path with the name of the corpus
copusPath = "/user/jeremy/spark_corpus/enwiki-latest-pages-articles-ASCII"

# Full path
modelPath = "model/mllib/enwiki_ASCII_K100_PART4_ALPHA05_iter1_Min5_Win10_2.model"
topNum = 20
seed = 42
itera = 1
#alpha = 0.025
alpha = 0.05
minCount = 5
window = 10
numPart = 4

# vector dimensionality word2vec
k = 100

############################
# Create the spark context #
############################

conf = SparkConf().setAppName("Build_MLLIB_MODEL_App")

sc = SparkContext(conf=conf)

####################
# Create the model #
####################

inp = sc.textFile(copusPath).map(lambda row: row.split(" "))


# SET The word2vec model 
# https://spark.apache.org/docs/2.1.0/api/java/org/apache/spark/mllib/feature/Word2Vec.html
word2vec = Word2Vec().setVectorSize(k).setSeed(seed).setNumIterations(itera).setLearningRate(alpha).setNumPartitions(numPart).setMinCount(minCount).setWindowSize(window)

model = word2vec.fit(inp)


####################
# Saved  the model #
####################

model.save(sc, modelPath)

# res = model.findSynonyms('china', topNum)
# print("china")
# print(res)

# res = model.findSynonyms('france', topNum)
# print("france")
# print(res)

# res = model.findSynonyms('portugal', topNum)
# print("portugal")
# print(res)


####################
# Load   the model #
####################

# sameModelLoad = Word2VecModel.load(sc, modelPath)

# res = sameModelLoad.findSynonyms('china', topNum)
# print("china")
# print(res)

# res = sameModelLoad.findSynonyms('france', topNum)
# print("france")
# print(res)

# res = sameModelLoad.findSynonyms('portugal', topNum)
# print("portugal")
# print(res)


# s = ('france', 'paris', 'portugal')
# print(getAnalogy(s, model))
# # u'lisbon'

# while(1):
# 		x = raw_input("x : ")
# 		y = raw_input("y : ")
# 		xprime = raw_input("x' : ")
# 		s = (x, y, xprime)
# 		print(getAnalogy(s, model))
