#!/usr/bin/env python
# -*- coding: utf8 -*-

from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext
from gensim import utils
import gensim, multiprocessing
from gensim.models.keyedvectors import KeyedVectors

# Transform the model in millib to gensim model 
# time spark-submit --conf spark.driver.maxResultSize=4G --executor-memory 24G  --driver-memory 24G Optional_Millib_Model_To_Gensim_Model.py 
# Increase the size of executor-memory and driver memory if this error appear : 
#		data = self._sock.recv(left) socket.timeout: timed out spark
#time spark-submit --conf spark.driver.maxResultSize=4G --executor-memory 24G  --driver-memory 24G Optional_Millib_Model_To_Gensim_Model.py 

############################
#       Config Part        #
############################

pathNameFileModel = "enwiki_ASCII_K100_PART4_ALPHA05_iter1_Min5_Win10.model"

pathLoadModel = pathNameFileModel + "/data/"

############################
# Create the spark context #
############################

conf = SparkConf().setAppName("Translate")
conf.set("spark.executor.heartbeatInterval","3600s")
sc = SparkContext('local[4]', '', conf=conf)
sqlContext = SQLContext(sc)

df = sqlContext.read.parquet(pathLoadModel)

############################
# Create and collect data  #
############################

pathNameFileModelTXT = pathNameFileModel + ".txt"
f = open(pathNameFileModelTXT, 'w')
df = df.collect()

#################################################
# Created and Saved the model in new format txt #
#################################################

i=0
for word,vect in df:
	if(i == 0):
		res = str(len(df))+ ' ' + str(len(vect)) + '\n'
		res = res.encode("utf8")
		#res = res.decode("utf8")
		f.write(res)
	list_to_string_vect = ' '.join(str(e)[0:9] if e < 0 else str(e)[0:8] for e in vect)
	res = word + ' ' + list_to_string_vect +'\n'
	res = res.encode("utf8")
	#res = res.decode("utf8")
	f.write(res)
	i = i + 1

##############################################
# Created and Saved the model in bin, gensim #
##############################################

# We load the previous model in text in order to create the another in .bin with gensim

model_load = KeyedVectors.load_word2vec_format(pathNameFileModelTXT, binary=False, encoding='utf8')
# ValueError: invalid vector on line 2104963 (is this really the text format?) -> this error means encoding error
#model_load = KeyedVectors.load_word2vec_format(pathNameFileModelTXT, binary=False)
pathNameFileModelBin = pathNameFileModel + ".bin"
# model_load.wv.save_word2vec_format(pathNameFileModelBin, binary=True)
model_load.save_word2vec_format(pathNameFileModelBin, binary=True)

f.close()
sc.stop()

print("END")


